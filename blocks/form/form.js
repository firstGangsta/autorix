//с самого начала скрываем все дополнительные фильтры
$('.form__detailed').hide()

//нажатие на название вкладки с фильтрами: автомобили, спецтехника, автозапчасти
$(".form__tabs-title").click(function () {
    //убираем у всех вкладок синюю подсветку
    $(".form__tabs-title").removeClass("active");
    //добавляем синюю подсветку только нажатой вкладке
    $(this).addClass("active");

    //прячем все контейнеры с фильтрами
    $(".form__content").hide();
    //показываем только тот контейнер с фильтрами, у которого data-tab атрибут равен data-tab атрибуту нажатой вкладки
    $(".form__content[data-tab=" + $(this).attr('data-tab') + "]").show();
})

//нажатие на "Детальный фильтр"
$(".form [data-role=toggleform]").click(function(){
    //Берем текущий контейнер с фильтрами
    let form = $(this).closest(".form__content");

    //и у всех фильтров, которые скрываются/показываются, переключаем видимость на противоположную
    $(form).find('.form__detailed').toggle();

    //кнопке "поиск" переключаем класс, чтобы она сместилась вниз или вернулась вверх
    $(form).find('[data-role=search]').toggleClass('formOpen')

    //если у нажатой кнопки надпись "Детальный фильтр" - меняем ее на "свернуть", а стрелку сбоку переворачиваем
    if ($(this).text() === 'Детальный фильтр'){
        $(this).text("Свернуть");
        $(this).removeClass("link_arrow-bottom");
        $(this).addClass("link_arrow-up");
    }
    //иначе - меняем надпись на "Детальный фильтр"
    else {
        $(this).text("Детальный фильтр");
        $(this).addClass("link_arrow-bottom");
        $(this).removeClass("link_arrow-up");
    }

})


//СТРАНИЦА "Добавление автомобиля"
//нажатие на "Добавить точный адрес"
$("[data-role='addAddress']").click(function() {
    //показываем поле вода точного адреса
    $(this).next().show();
    //прячем саму надпись "Добавить точный адрес"
    $(this).hide();
})

//ввод цены - форматирование числа вида 1000000 в 1 000 000
$('[data-role=price]').on("propertychange keyup input paste", function() {
    let formated = parseInt($(this).val().replace(/\s/g,"")).toLocaleString();
    if (Number.isInteger(parseInt(formated)))
        $(this).val(formated);
})

// загрузка множества файлов
function handleFileSelect(evt) {
  var files = evt.target.files; // FileList object
  if (files.length > 20 || ($(".form__loaded-img").length + files.length > 20)){
      alert("Больше 20 файлов загрузить нельзя!")
      return;
  }

  // Loop through the FileList and render image files as thumbnails.
  for (var i = 0, f; f = files[i]; i++) {

    // Only process image files.
    if (!f.type.match('image.*')) {
      continue;
    }

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
      return function(e) {
        // Render thumbnail.
        var span = document.createElement('span');
        span.innerHTML = ['<img class="form__loaded-img" src="', e.target.result,
                          '" title="', encodeURI(theFile.name), '"/>'].join('');
        document.querySelector('.form__photos').insertBefore(span, null);
      };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsDataURL(f);
  }
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);

$("[type=checkbox][data-role=exchange]").change(function() {
    if ($(this).is(":checked")){
        $('input[type=text][data-role=exchange]').removeAttr("disabled")
    }
    else {
        $('input[type=text][data-role=exchange]').attr("disabled",'true');
        $('input[type=text][data-role=exchange]').val("")
    }
})
