$('.link[data-modal=cities]').click(function(){
    $(".modal-background").show();
    $(".modal-window[data-modal=cities]").show();
})

$(".modal-background, .modal-window__close").click(function() {
    $(".modal-window[data-modal=cities]").hide();
    $(".modal-background").hide();
})
