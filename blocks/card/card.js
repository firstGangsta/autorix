//обработчик нажатия на превью
$(".card__preview").click(function() {

    //удаляем активный класс у предыдущего выбранного превью
    $(this).parent().find('.card__preview_active').removeClass("card__preview_active");

    //добавляем активный класс тому превью, на которое нажали
    $(this).addClass("card__preview_active");

    //устанавливаем выбранное превью в качестве основного изображения товара
    $('.card__img img').attr("src", $(this).attr("src"));

    //получаем индекс превью, на которое нажали
    let clickedIndex = $(this).attr("data-index");

    //установить специальный атрибут слайдера, равный текущему индексу
    $('.card__img img').attr("data-current", clickedIndex);
});

//обработчик нажатия на стрелку слайдера "влево"
$(".card__img-prev").click(function() {
    //узнать, сколько всего картинок в слайдере
    let totalCount = $(".card__preview").length;

    //получить индекс текущего выбранного изображения
    let clickedIndex = $('.card__img img').attr("data-current");

    //отнять от него единицу
    let newIndex = parseInt(clickedIndex) - 1;

    //если полученное число меньше 0
    if (newIndex < 0)
        //установить эту переменную равной индексу последней картинки (отнимаем 1, потому что нумерация начинается с нуля)
        newIndex = totalCount - 1;

    //получить путь картинки, которую сейчас нужно показать
    let newImgPath = $(".card__preview[data-index="+newIndex+"]").attr("src");

    //установить изображение в слайдер
    $('.card__img img').attr("src", newImgPath);

    //установить специальный атрибут слайдера, равный текущему индексу
    $('.card__img img').attr("data-current", newIndex);

    //установить в списке превью активным тот элемент, который сейчас отображается (черная полоса справа)
    $(".card__preview").removeClass("card__preview_active");
    $(".card__preview[data-index="+newIndex+"]").addClass("card__preview_active");
});


//обработчик нажатия на стрелку слайдера "вправо"
$(".card__img-next").click(function() {
    //узнать, сколько всего картинок в слайдере
    let totalCount = $(".card__preview").length;

    //получить индекс текущего выбранного изображения
    let clickedIndex = $('.card__img img').attr("data-current");

    //отнять от него единицу
    let newIndex = parseInt(clickedIndex) + 1;

    //если полученное число меньше 0
    if (newIndex >= totalCount)
        //установить эту переменную равной индексу последней картинки (отнимаем 1, потому что нумерация начинается с нуля)
        newIndex = 0;

    //получить путь картинки, которую сейчас нужно показать
    let newImgPath = $(".card__preview[data-index="+newIndex+"]").attr("src");

    //установить изображение в слайдер
    $('.card__img img').attr("src", newImgPath);

    //установить специальный атрибут слайдера, равный текущему индексу
    $('.card__img img').attr("data-current", newIndex);

    //установить в списке превью активным тот элемент, который сейчас отображается (черная полоса справа)
    $(".card__preview").removeClass("card__preview_active");
    $(".card__preview[data-index="+newIndex+"]").addClass("card__preview_active");
});
