$(".car-carousel").owlCarousel({
    items:1,
    loop:true,
    margin:10,
    nav:true,
    dots:false,
    navText: ["<img src='img/car-carousel-left-arrow.png' alt='влево'>","<img src='img/car-carousel-right-arrow.png' alt='вправо'> "],
    responsive : {
          1920 : {
             items:7
          },
          1650 : {
             items:6
          },
          1266 : {
             items:5
          },
          1066 : {
             items:4
         },
          800:{
              items: 3
          },
          500:{
              items: 2
          },
          350:{
              items: 1
          }
      }
})
